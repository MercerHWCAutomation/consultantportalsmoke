Feature: Broker Portal Smoke Test cases

  ##Smoke @ClientWizardViewClientSetup
  #Scenario: Accessing the Client Wizard View page Individually
  # Given   User opens the browser and launch the Broker Portal URl
  # And    Enter valid username and password and click Login on Broker Portal Login Page
  # And    Broker Portal Dashboard should be displayed
  # When   User opens and fills Client Profile Page
  # And    User able to navigates and fills Plan decision Page
  # And    User able to navigates and fills Benefits Page
  # And    User able to navigate and fills the Contribution Page
  # And    User able to navigate and fills the Rates Page
  # And    User able to navigate and fills the Adminstration Page
  # And    User able to navigate and fills Employee Data Page
  # And    User able to navigate and fills Account Structure Page
  # And    User able to navigate and fills the Validation Page
  # Then   User able to successfully complete the setup file

  @Smoke @BrokerPortalLoginFunctionality @tempqw
  Scenario: Checking the Login flow for the Broker Portal
    Given   User opens the browser and launch the Broker Portal URl
    When    Enter valid username and password and click Login on Broker Portal Login Page
    Then    Broker Portal Dashboard should be displayed

  #@Smoke @EducationFunctionality        #Hovering and Active X issues
  #Scenario: Checking the Education Video Tab
  #  Given   User opens the browser and launch the Broker Portal URl
  #  And    Enter valid username and password and click Login on Broker Portal Login Page
  #  And    Broker Portal Dashboard should be displayed
  #  #Given User is on Broker Portal Dashboard Page
  #  When  User clicks on the Education Tag from Resources Tab
  #  Then  User should able to check the Video Page is Opened

  @Smoke @AccessToHRPortal @tempqw
  Scenario: Accessing the proxy HR portal
    Given   User opens the browser and launch the Broker Portal URl
    And    Enter valid username and password and click Login on Broker Portal Login Page
    And    Broker Portal Dashboard should be displayed
    When   User opens the active clients list
    And    User opens HR Admin Proxy for an active clients
    Then   User should able to access HR Proxy site
    And    User get backs to Broker Application once HR Proxy site closed
    #And    User opens Ready client list
    #And    User opens HR Admin Proxy for an Ready clients
    #And    User should able to access HR Proxy site for Ready Client
    #And    User get backs to Broker Application once HR Proxy site closed

  @Smoke @DocumentLibrary
  Scenario: Accessing the Document Library Section
    Given   User opens the browser and launch the Broker Portal URl
    And    Enter valid username and password and click Login on Broker Portal Login Page
    And    Broker Portal Dashboard should be displayed
    Then   User opens the Document Library Page and checks the default view


  @Smoke @LogoutFunctionality
  Scenario: Accessing the Logout Functionality
    Given   User opens the browser and launch the Broker Portal URl
    And    Enter valid username and password and click Login on Broker Portal Login Page
    And    Broker Portal Dashboard should be displayed
    When   User able to click logout button
    Then   User should get logged out successfully

  @Smoke @DashboardVerification
  Scenario: Accessing the Dashboard page Section
    Given   User opens the browser and launch the Broker Portal URl
    And    Enter valid username and password and click Login on Broker Portal Login Page
    And    Broker Portal Dashboard should be displayed
    When   Pending and Active clients tabs should be displayed
    Then   Other Links on the Dashboard Pages should be displayed




