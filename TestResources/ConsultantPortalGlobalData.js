/**
 * Created by subhajit-chakraborty on 4/25/2017.
 */

module.exports = {

    //Specify Your Testing Environment

    //TestingEnvironment: 'QAF',
    //TestingEnvironment: 'CITEST',
   //TestingEnvironment: 'PROD',
     TestingEnvironment: 'CSO',


    shortWait: 5000,
    longWait: 10000,
    LoginWait: 30000,
    ProfileCreationTime:60000,

    //Environment User details

    //QAF
     BrokerPortalURL: 'https://consultant-qaf.mercermarketplace365plus.com',
     BrokerPortalUsername: 'qam.sneha.mhetre@mercer.com',
     BrokerPortalPassword: 'sneha@1234',
      //BrokerPortalUsername: 'qa41.triveni.gherde@gisqa.mercer.com',
     //BrokerPortalPassword: 'Mercer1!',

    //Prod
   //BrokerPortalURL: 'https://consultant.mercermarketplace365plus.com',
   //BrokerPortalUsername: 'Barbara.meldrum@analystmercer.com',
   //BrokerPortalPassword: 'Mercer6!',

    //CSO
    //BrokerPortalURL: 'https://consultant-cso.mercermarketplace365plus.com/Account/Login',
    //BrokerPortalUsername: '4.arun.rajendiran@gisqa.mercer.com',
    //BrokerPortalPassword: 'Mercer09@',

    //CITest
    //BrokerPortalURL: 'https://consultant-cit.mercermarketplace365plus.com/Account/Login',
    //BrokerPortalUsername: 'qacit.triveni.gherde@gisqa.mercer.com',
    //BrokerPortalPassword: 'Mercer09@',

    //Client Name :
    ClientName: 'DR Test Client',



    //Client Wizard View:
    Address1USHeadQuarter: '15th Street',
    CityUSHeadquarter: 'Arizona',
    ZipCodeUSHeadquarter: '96354',

    //HR Details
    HRName: 'DemoHR',
    HREmail: 'hr1@abc.com',
    HRContactNumber: '9654123870',

    //Client Profile 2nd Page
    EnrollmentStartDate: '07/14/2017',
    PlanYearEffDate: '08/01/2017',
    FederalTaxID: '658792541',
    ClientOneCode: '659823',

    //Payroll Freequency
    WeeklyDate: '07/07/2017',
    PendingCobraMember: '1',
    EnrolledCobraMem: '1',

    //Upload Documents
    //RatesDocumentPath: 'C:/Users/raghun_app/Desktop/Consultant portal data for auto run/QAF/rates_8575',
    EmployeeDetailsPath:'C:/Users/raghun_app/Desktop/Consultant portal data for auto run/QAF/EmployeeDataFileQA',






}
